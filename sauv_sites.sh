#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

# Définition du dossier d'installation
DOSSIER_INSTALL=/opt/e-comBox
BRANCHE=dev

# Récupération sur gitlab du fichier de fonctions
curl -fsSL https://gitlab.com/e-combox/e-comBox_scriptsLinux/raw/"$BRANCHE"/fonctions.sh -o $DOSSIER_INSTALL/fonctions.sh

# Appel des fichiers de fonctions et de param.conf
source $DOSSIER_INSTALL/fonctions.sh
source $DOSSIER_INSTALL/param.conf
#source ./fonctions.sh

SAUV_SITES
