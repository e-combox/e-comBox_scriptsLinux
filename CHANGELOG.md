# Changelog | Liste des changements

## [4.1.0] (2023-02-21)

### Installation

* **Installation facilitée** : installation automatisée (sans interaction avec l'administrateur).
* **Déploiement via Ansible** : exemples de playbook permettant un déploiement automatisé de l'application.

### Gestion de l'application

* **Script delete_application.sh** : permet de désinstaller proprement l'application et éventuellement Docker et Docker Compose.
* **Script reinitialise_application.sh** : permet de réinitialiser l'application pour repartir sur un environnement "propre".
* **Script manage_certificats.sh** : permet de créer un certificat Let's Encrypt et de mettre à jour les certificats existants.
* **Script manage_images.sh** : permet de supprimer et de mettre à jour les images.

## [4.0.0] (2022-11-20)

### Installation

* **Reverse Proxy** : possibilité de passer par un Reverse Proxy de manière à ne pas être obligé d'exposer des ports (mis à part le port d'accès à l'interface) sur le serveur et à ne dédier qu'une seule adresse IP publique pour l'ensemble des instances.

### Gestion de l'application

* **Portainer** : synchronisation automatique de l'application avec le mot de passe défini par l'administrateur dans Portainer.
* **Script reset_pass_portainer** : permet de réinitialiser le mot de passe de Portainer.

### Sécurité

* **HTTPS** : intégration du protocole HTTPS au niveau des sites et de toute l'application (Portainer et e-comBox).

## [3.0.0] (2021-07-06)

### Gestion de l'application

* **Sauvegarde/restauration** globale (en demandant un chemin à l’utilisateur).

## [2.0.0] (2020-12-18)

### Sécurisation des sites et de l'application

* Accès aux sites à partir d’une URL (par exemple : <http://adresseIP:troisièmePort/nom_site>) en masquant la redirection vers les différents ports via un reverse proxy : seuls 3 ports plus les ports pour le SFTP (si ce dernier est utilisé) allant de 2200-2299 doivent être ouverts via un pare-feu.
* Choix possible des ports utilisés par l’application. Les ports 8800 (pour l'accès au sites), 8880 (pour l'accès à l'administration avancée) et 8888 (pour l'accès à l'application) sont proposés par défaut.
* Les mots de passe n'apparaissent plus dans les Docker Compose.
* Accès à distance à l'interface d'e-comBox avec possibilité de créer un nom d'utilisateur et un mot de passe.
* Modification possible du mot de passe du compte "admin" de Portainer (administration avancée).
