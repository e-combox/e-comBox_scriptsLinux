#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

# Définition du dossier d'installation
DOSSIER_INSTALL=/opt/e-comBox
source $DOSSIER_INSTALL/variables.sh

# Détermination de la date d'expiration du certificat pour nginx
date_exp=$(date -d "$(openssl x509 -in "$DOSSIER_CERTS"/ecombox.crt -noout  -enddate | cut -d"=" -f2)" +"%s")

# On renouvelle le certificat s'il expire dans moins d'un jour.
date_test=$(("$date_exp" - 24*3600))  
date_auj=$(date +%s)
if [ "$date_auj" -ge "$date_test" ]; then
   bash $DOSSIER_INSTALL/manage_certificat.sh -m
fi