#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

##########################################
# MIGRATION DES SOURCES VERS LA FORGE AIEF
##########################################

# Définition de variables utiles avant le chargement du fichier variables.sh
COLTITRE="\033[1;35m" # Rose
COLCMD="\033[1;37m"   # Blanc
DOSSIER_INSTALL=/opt/e-combox
DEPOT_GITLAB_SCRIPTS='https://forge.aeif.fr/e-combox/e-combox_scriptslinux'
BRANCHE="4.1"
VERSION_APPLI="4.1"
VERSION_SCRIPT="4.1.1"
LOGFILE="/var/log/ecombox.log"

SHOW_USAGE() {
   echo -e "\nCe script permet d'installer l'application e-comBox et peut être utilisé avec des options facultatives qui remplacent les valeurs du fichier param.conf."
   echo -e "La valeur des principaux paramètres peut être passée directement en ligne de commande (voir Usage ci-après)."
   echo -e "Le mot de passe ne peut pas contenir les caractères \" et \$."
   echo -e "À noter que l'utilisation de l'option \"f\" permet de passer l'ensemble des paramètres qui doivent être modifiés dans param.conf.\n"
   echo -e "Usage: configure_application.sh [-f \"valeur\"] [-i \"valeur\"] [-d \"valeur\"] [-r \"valeur\"] [-c \"valeur\"] [-p \"valeur\"] [-h]"
   echo -e "\t-f\t\tChemin vers le fichier de paramètre  [-f \"/chemin/nom_fichier\"]"
   echo -e "\t-i\t\tAdresse IP privée ou nom de domaine correspondant à une adresse IP privée  [-i \"@IP_PRIVEE\" | -i \"nom_domaine\"]"
   echo -e "\t-d\t\tAdresse IP publique ou nom de domaine correspondant à une adresse IP publique  [-d @IP_PUBLIQUE | -d nom_domaine]. Pour supprimer une adresse IP publique ou un domaine existant -d \"\""
   echo -e "\t-r\t\tPassage par un Reverse-Proxy externe ou non  [-r \"O\" | -r \"N\"]"
   echo -e "\t-c\t\tChemin en cas de Reverse-Proxy externe [-c \"chemin\"]. Pour supprimer un chemin existant -c \"\""
   echo -e "\t-p\t\tMot de passe de Portainer  [-p \"mot_de_passe\"]. Les caractères suivants \" $ \` \\ & \| ! [space] ne peuvent pas être utilsés en ligne de commande."
   echo -e "\t-s\t\tMet la variable de suppression des images à \"true\" le temps d'exécution du script [-s]"
   echo -e "\t-b\t\tMet la variable de suppression des images à \"false\" le temps d'exécution du script [-n]"
   echo -e "\t-h\t\tDétail des options"
}

#echo "Le nombre d'argument est $#"
nombre_param=$#

if [ $# -eq 0 ]; then
   #echo -e "\n$COLINFO Aucune option passée en ligne de commande.$COLCMD\n"
   echo -e "\nAucune option passée en ligne de commande.\n" >>"$LOGFILE"
fi

# Initialisation des paramètres passés au script
while getopts ":f:i:d:r:c:p:sbh" option; do
   # f: fichier param.conf
   # i: adresse ip privée
   # d: domaine
   # r: passage par un reverse-proxy
   # c: chemin
   # p: mot de passe de portainer
   # s: met la variable "DEL_IMAGES à "true" le temps de l'exécution du script
   # b: met la variable "DEL_IMAGES à "false" le temps de l'exécution du script
   # h: help

   case $option in
   f) fichier_param_o=$OPTARG ;;
   i) adresse_ip_privee_o=$OPTARG ;;
   d)
      if [ -z "$OPTARG" ] || [ "$OPTARG" = "" ]; then
         domaine_o="NULL"
      else
         domaine_o=$OPTARG
      fi
      ;;
   r) rp_ext_o=$OPTARG ;;
   c)
      if [ -z "$OPTARG" ] || [ "$OPTARG" = "" ]; then
         chemin_o="NULL"
      else
         chemin_o=$OPTARG
      fi
      ;;
   p)
      mdp_portainer_o=$OPTARG
      mdp_portainer_o=${mdp_portainer_o/\\/\\\\\\\\}
      mdp_portainer_o=${mdp_portainer_o/&/\\&}
      mdp_portainer_o=${mdp_portainer_o/|/\\|}
      mdp_portainer_o=${mdp_portainer_o/!/\\!}
      ;;
   s) del_images_o="true" ;;
   b) del_images_o="false" ;;
   h)
      SHOW_USAGE
      exit
      ;;
   \?)
      echo -e "\n$COLSTOP $OPTARG : option invalide.$COLCMD\n"
      echo -e "\n$COLSTOP $OPTARG : option invalide.$COLCMD\n" >>"$LOGFILE"
      SHOW_USAGE
      exit 1
      ;;
   esac
done
#shift "$($OPTIND - 1)"

#clear
echo -e "$COLTITRE"
echo "**********************************************************"
echo "             INSTALLATION DE L'E-COMBOX $VERSION_APPLI "
echo "         ET CONFIGURATION DE SON ENVIRONNEMENT                       "
echo "**********************************************************"
echo -e "$COLCMD"

# Installation de curl si besoin
if ! (curl -h >/dev/null 2>&1); then
   echo -e "Téléchargement et installation de curl... En cours"
   apt-get update
   apt-get install -y curl
else
   echo -e "Paquet curl... Installé"
fi

# Mise en place des dossiers et fichiers

# Création du dossier d'installation de l'application s'il n'existe pas déjà
if [ ! -d "$DOSSIER_INSTALL" ]; then
   # Le nom du dossier d'installation a été modifié
   if [ -d "/opt/e-comBox" ]; then
      mv /opt/e-comBox $DOSSIER_INSTALL
   else
      mkdir $DOSSIER_INSTALL
   fi
fi

# Téléchargement du fichier des variables et des fonctions
curl -fsSL "$DEPOT_GITLAB_SCRIPTS"/raw/"$BRANCHE"/variables.sh -o $DOSSIER_INSTALL/variables.sh
curl -fsSL "$DEPOT_GITLAB_SCRIPTS"/raw/"$BRANCHE"/fonctions.sh -o $DOSSIER_INSTALL/fonctions.sh

# Appel des fichiers de variables et fonctions
source $DOSSIER_INSTALL/variables.sh
source $DOSSIER_INSTALL/fonctions.sh
#source ./variables.sh
#source ./fonctions.sh

# Création des dossiers qui vont stocker en local une copie des certificats
if [ ! -d "$DOSSIER_CERTS/registry" ]; then
   mkdir -p $DOSSIER_INSTALL/certs/registry
fi

# Création ou réinitialisation du fichier de log (avec rotation sur 4 fichiers)
if [ -e "$LOGFILE" ]; then
   BACKUP "$LOGFILE"
fi
echo -e "Configuration d'e-comBox le $(date)\n avec la version du script $VERSION_SCRIPT." >"$LOGFILE"

# Vérification de l'installation de Docker et la version de Docker Compose
if ( ! (docker -v >>"$LOGFILE" 2>&1 && docker-compose -v >>"$LOGFILE" 2>&1)); then
   echo -e "$COLDEFAUT"
   echo -e "Installation de Docker et Docker-compose"
   echo -e "$COLCMD"
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/install_docker_docker-compose.sh -o install_docker_docker-compose.sh
   bash install_docker_docker-compose.sh 2>>"$LOGFILE"
   rm install_docker_docker-compose.sh
   echo -e "docker et docker-compose viennent d'être installés" >>"$LOGFILE"
   echo -e "$COLCMD"
   #else
   #version_dc=$(docker-compose -v | cut -d" " -f4)
   #if ! [ "$version_dc" = "v2.15.1" ]; then
   #curl -L "https://github.com/docker/compose/releases/download/v2.15.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose >>$LOGFILE 2>&1
   #chmod +x /usr/local/bin/docker-compose
   #if (docker-compose -v >/dev/null); then
   #echo -e "Installation de la nouvelle version de Docker Compose... succès"
   #else
   #echo -e "$COLSTOP"
   #echo -e "Installation de la nouvelle version de Docker Compose... echec$COLCMD"
   #exit
   #fi
   #fi
fi

initInstall() {

   echo -e "$COLDEFAUT"
   echo -e "Initialisation des paramètres du fichier param.conf"
   echo -e "Initialisation des paramètres du fichier param.conf\n" >>"$LOGFILE"
   echo -e "$COLCMD"
   # Si aucun fichier paramètre n'est passé lors de l'exécution du script
   if [ -z "$fichier_param_o" ]; then
      # Si aucun param.conf n'existe dans le dossier d'installation on le télécharge depuis gitlab
      if [ ! -e "$FICHIER_PARAM" ]; then
         # Téléchargement du fichier contenant les paramètres par défaut et restriction des droits
         curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/param.conf -o "$FICHIER_PARAM"
         chmod 600 "$FICHIER_PARAM"
         echo -e "Le fichier param.conf est absent de $DOSSIER_INSTALL, il a été téléchargé à l'adresse $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/param.conf avec les paramètres par défaut." >>"$LOGFILE"
         echo -e "Fichier param.conf absent... Téléchargé"
         if [ "$nombre_param" -eq 0 ]; then
            SHOW_USAGE
            echo -e "\n$COLINFO Aucune option passée en ligne de commande.$COLCMD\n" >>"$LOGFILE"
            echo -e "\n$COLSTOP Vous n'aviez pas de fichier param.conf dans le $DOSSIER_INSTALL et vous n'avez pas passé de fichier en paramètre ni d'options qui aurait permis de compléter le fichier téléchargé$COLCMD".
            echo -e "\nVeuillez le compléter, notamment les paramètres obligatoires ADRESSE_IP_PRIVEE et MDP_PORTAINER, puis relancez le script."
            exit 1
         fi

      else
         chmod 600 "$FICHIER_PARAM"
         echo -e "Fichier param.conf... Présent"
      fi
   else
      if [ "$fichier_param_o" != "$FICHIER_PARAM" ]; then
         BACKUP "$FICHIER_PARAM"
         cp "$fichier_param_o" "$FICHIER_PARAM"
         chmod 600 "$FICHIER_PARAM"
         echo -e "Fichier param.conf... Prise en compte des paramètres"
      fi
   fi

   # Traitement des options passées en paramètre
   if [ -n "$adresse_ip_privee_o" ]; then
      if (cat <"$FICHIER_PARAM" | grep "ADRESSE_IP_PRIVEE" >/dev/null); then
         sed -i "s|^ADRESSE_IP_PRIVEE=.*|ADRESSE_IP_PRIVEE=\"$adresse_ip_privee_o\"|" "$FICHIER_PARAM"
      else
         echo "ADRESSE_IP_PRIVEE=\"$adresse_ip_privee_o\"" >>"$FICHIER_PARAM"
      fi
   fi

   if [ -n "$domaine_o" ]; then
      if (cat <"$FICHIER_PARAM" | grep "DOMAINE" >/dev/null); then
         if [ "$domaine_o" = "NULL" ]; then
            # Le paramètre domaine doit être forcé à vide
            sed -i "s|^DOMAINE=.*|DOMAINE=\"\"|" "$FICHIER_PARAM"
         else
            sed -i "s|^DOMAINE=.*|DOMAINE=\"$domaine_o\"|" "$FICHIER_PARAM"
         fi
      else
         echo "DOMAINE=\"$domaine_o\"" >>"$FICHIER_PARAM"
      fi
   fi

   if [ -n "$rp_ext_o" ]; then
      if (cat <"$FICHIER_PARAM" | grep "RP_EXT" >/dev/null); then
         sed -i "s|^RP_EXT=.*|RP_EXT=\"$rp_ext_o\"|" "$FICHIER_PARAM"
      else
         echo "RP_EXT=\"$rp_ext_o\"" >>"$FICHIER_PARAM"
      fi
   fi

   if [ -n "$chemin_o" ]; then
      if (cat <"$FICHIER_PARAM" | grep "CHEMIN" >/dev/null); then
         if [ "$chemin_o" = "NULL" ]; then
            # Le paramètre chemin doit être forcé à vide
            sed -i "s|^CHEMIN=.*|CHEMIN=\"\"|" "$FICHIER_PARAM"
         else
            sed -i "s|^CHEMIN=.*|CHEMIN=\"$chemin_o\"|" "$FICHIER_PARAM"
         fi
      else
         echo "CHEMIN=\"$chemin_o\"" >>"$FICHIER_PARAM"
      fi
   fi

   if [ -n "$mdp_portainer_o" ]; then
      if (cat <"$FICHIER_PARAM" | grep "MDP_PORTAINER" >/dev/null); then
         # Debug
         #echo "Le mot de passe passé dans start_configure_application est $mdp_portainer_o"
         sed -i "s|^MDP_PORTAINER=.*|MDP_PORTAINER=\"$mdp_portainer_o\"|" "$FICHIER_PARAM"
      else
         echo "MDP_PORTAINER=\"$mdp_portainer_o\"" >>"$FICHIER_PARAM"
      fi
   fi

   source "$FICHIER_PARAM"
   #source ./param.conf

   # Vérification des paramètres du fichier param.conf
   VERIF_PARAM

   # Vérification de la version installée
   verifVersion
}

installer() {

   # Initialisation
   echo -e "$COLDEFAUT"
   echo -e "Initialisation"
   echo -e "$COLCMD"

   if ! (jq -h >/dev/null 2>&1); then
      echo -e "Paquet \"jq\" manquant... Installation"
      apt update
      apt install -y jq
      echo -e "$COLCMD"
   else
      echo -e "Paquet \"jq\"... Déjà installé"
   fi

   # Création du fichier config.json s'il n'existe pas
   if [ ! -e ~/.docker ]; then
      mkdir ~/.docker
   fi

   if [ ! -e ~/.docker/config.json ]; then
      echo "" >~/.docker/config.json
      chown -R "$USER":docker ~/.docker
      chmod g+rw ~/.docker/config.json
      echo -e "Création d'un fichier vide config.json... Fait"
   fi

   #Configuration du proxy pour GIT et pour Docker
   if [ "$ADRESSE_PROXY" != "" ]; then
      echo -e "$COLDEFAUT"
      echo -e "Congiguration de GIT pour le proxy"
      echo -e "$COLCMD\c"
      git config --global http.proxy "$ADRESSE_PROXY"
      if [ ! -d "/etc/systemd/system/docker.service.d" ]; then
         mkdir /etc/systemd/system/docker.service.d
      fi
      echo -e "$COLCMD\c"
      {
         echo "[Service]"
         echo "Environment=\"HTTP_PROXY=http://$ADRESSE_PROXY\""
         echo "Environment=\"HTTPS_PROXY=http://$ADRESSE_PROXY\""
         echo "Environment=\"NO_PROXY=$NO_PROXY\""
      } >/etc/systemd/system/docker.service.d/http-proxy.conf
      echo -e "Ajout des variables d'environnement à systemd... Fait"
      echo -e "Ajout des variables d'environnement à systemd (/etc/systemd/system/docker.service.d/http-proxy.conf)." >>"$LOGFILE"
      # Redémarrage de Docker
      systemctl daemon-reload
      systemctl restart docker
      echo -e "\nRedémarrage de Docker... Succès"
      echo -e "\nRedémarrage de Docker." >>"$LOGFILE"

      # Sauvegarde du fichier config.json d'origine si ce n'est déjà fait
      if [ ! -e ~/.docker/config.json.ori ]; then
         cp ~/.docker/config.json ~/.docker/config.json.ori
         echo -e "Sauvegarde du fichier ~/.docker/config.json d'origine... Fait"
      fi
      # Sauvegarde du fichier actuel et restauration du fichier d'origine
      cp ~/.docker/config.json ~/.docker/config.json.sauv
      echo -e "Sauvegarde du fichier ~/.docker/config.json actuel... Fait"
      cp ~/.docker/config.json.ori ~/.docker/config.json
      echo -e "Restauration du fichier ~/.docker/config.json d'origine... Fait"
      #echo "" > ~/.docker/config.json
      #chown -R $USER:docker ~/.docker
      #chmod g+rw ~/.docker/config.json

      # Ajout des paramètres du proxy au fichier config.json
      if [ "$(sed -n '$=' /root/.docker/config.json)" = 1 ] || [ -z "$(sed -n '$=' /root/.docker/config.json)" ]; then
         echo -e "le fichier config.json est bien vide." >>"$LOGFILE"
         sed -i "1i \ {\n\t\"proxies\": {\n\t  \"default\":\n\t  {\n\t\t\"httpProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"httpsProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"noProxy\": \"$NO_PROXY\"\n  }\n\t}\n}" ~/.docker/config.json
      else
         echo -e "le fichier config.json n'est pas vide" >>"$LOGFILE"
         sed -i "/\"proxies\": {/,10d" ~/.docker/config.json
         if [ "$(sed -n '$=' /root/.docker/config.json)" = 2 ]; then
            echo -e "le fichier config.json a 2 lignes." >>"$LOGFILE"
            sed -i "1a \ \t\"proxies\": {\n\t  \"default\":\n\t  {\n\t\t\"httpProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"httpsProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"noProxy\": \"$NO_PROXY\"\n  }\n\t}\n}" ~/.docker/config.json
         else
            echo -e "Le fichier config.json a plus de 2 lignes." >>"$LOGFILE"
            sed -i "1a \ \t\"proxies\": {\n\t  \"default\":\n\t  {\n\t\t\"httpProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"httpsProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"noProxy\": \"$NO_PROXY\"\n\t  }\n\t},\n" ~/.docker/config.json
         fi
      fi
      echo -e "Ajout des paramètres du Proxy dans ~/.docker/config.json... Fait"
      echo -e "Ajout des paramètres du proxy $ADRESSE_PROXY et $NO_PROXY dans ~/.docker/config.json.\n" >>"$LOGFILE"

   else
      echo -e "Proxy configuré sur le système... Aucun"
      echo -e "Aucun proxy configuré sur le système. Les paramètres du proxy, s'ils existent, sont supprimés.\n" >>"$LOGFILE"
      git config --global --unset http.proxy
      if [ -f "/etc/systemd/system/docker.service.d/http-proxy.conf" ]; then
         rm -f /etc/systemd/system/docker.service.d/http-proxy.conf
         systemctl daemon-reload
         systemctl restart docker
      fi
      # Sauvegarde du fichier actuel
      cp ~/.docker/config.json ~/.docker/config.json.sauv
      # Suppression des paramètres du proxy du fichier config.json
      sed -i "/\"proxies\": {/,9d" ~/.docker/config.json
      if [ "$(sed -n '$=' /root/.docker/config.json)" -le 3 ]; then
         #debug
         echo -e "Suppression éventuelle des paramètres du proxy du fichier config.json." >>"$LOGFILE"
         rm -rf ~/.docker/config.json
      fi
   fi

   # Création du réseau pour l'application si aucun réseau déjà créé ou si la valeur a changé dans param.conf
   if (docker network ls | grep bridge_e-combox >/dev/null); then
      NET_ECB_ACTUEL=$(docker network inspect --format='{{range .IPAM.Config}}{{.Subnet}}{{end}}' bridge_e-combox)
      if [ "$NET_ECB" ] && [ "$NET_ECB_ACTUEL" != "$NET_ECB" ]; then
         # shellcheck disable=SC2046
         docker rm -f $(docker ps -aq)
         # shellcheck disable=SC2046
         docker volume rm $(docker volume ls -qf dangling=true)
         docker network rm bridge_e-combox
         docker network create --subnet "$NET_ECB" bridge_e-combox >>"$LOGFILE" 2>&1
         echo -e "Création du nouveau réseau pour l'application ${COLCHOIX}$NET_ECB${COLCMD}... Fait"
         echo -e "Le nouveau réseau ${COLCHOIX}$NET_ECB${COLCMD} a été créé." >>"$LOGFILE"
      else
         echo -e "Vérification du réseau ${COLCHOIX}$NET_ECB${COLCMD} pour l'application... Rien à faire"
      fi
   else
      docker network create --subnet "$NET_ECB" bridge_e-combox >>"$LOGFILE" 2>&1
      if [ "$?" -eq 0 ]; then
         echo -e "Création du réseau ${COLCHOIX}$NET_ECB${COLCMD} pour l'application... Succès"
      else
         echo -e "$COLSTOP"
         echo -e "Création du réseau ${COLCHOIX}$NET_ECB${COLCMD} pour l'application... Échec"
         echo -e "$COLCMD"
         echo -e "Consultez les log \"$LOGFILE\".\n"
         echo -e "Le réseau pour l'e-comBox n'a pas pu être lancé." >>"$LOGFILE"
         exit
      fi
   fi

   echo -e "Le réseau $NET_ECB est utilisé pour l'application.\n" >>"$LOGFILE"

   # Reverse Proxy Nginx
   echo -e "$COLDEFAUT"
   echo -e "Configuration du reverse-proy"
   echo -e "$COLCMD"
   
   #Récupération du reverse proxy
   if [ -d "$DOSSIER_RP" ]; then
      echo -e "Le reverse-proxy existe et va être remplacé.\n" >>"$LOGFILE"
      cd "$DOSSIER_RP" || exit

      {
         docker-compose down
         # Suppression des volumes (sauf celui contenant les certificats)
         docker volume rm e-combox_reverseproxy_nginx-html
         docker volume rm e-combox_reverseproxy_nginx-docker-gen-templates
      } >>"$LOGFILE" 2>&1

      cd ..
      rm -rf "$DOSSIER_RP"
   fi

   cd $DOSSIER_INSTALL || exit
   echo -e "Téléchargement du reverse-proxy... En cours"
   git clone -b "$BRANCHE" "$DEPOT_GITLAB_RP_EXT".git >>"$LOGFILE" 2>&1
   if [ $? -eq 0 ]; then
      echo -e "Téléchargement du reverse-proxy... Succès"
   else
      echo -e "$COLSTOP"
      echo -e "Téléchargement du reverse-proxy... Échec"
      echo -e "$COLCMD"
      echo -e "Consultez les log \"$LOGFILE\".\n"
      echo -e "Le reverse proxy n'a pas pu être téléchargé." >>"$LOGFILE"
      exit
   fi

   # Ajout des variables d'environnement à Nginx
   RECUP_VARIABLES_ECB

   {
      echo -e "ADRESSE_IP_PRIVEE=$ADRESSE_IP_PRIVEE"
      echo -e "DOMAINE=$DOMAINE"
      echo -e "NGINX_HOST=$NGINX_HOST"
      echo -e "NGINX_PORT=$PORT_RP"
      echo -e "FQDN=$FQDN"
      echo -e "PORT_REGISTRY=$PORT_REGISTRY"
      echo -e "CHEMIN=$CHEMIN"
      #On a besoin du chemin absolu dans le docker compose du sftp et pour le index.html de l'application
      echo -e "CHEMIN_ABSOLU=$CHEMIN_ABSOLU"
      # Information sur la version du script
      echo -e "VERSION_SCRIPT=$VERSION_SCRIPT"

   } >"$DOSSIER_RP"/.env

   echo "Mise à jour de $DOSSIER_RP/.env... Fait"
   echo -e "Mise à jour de $DOSSIER_RP/.env... Fait.\n" >>"$LOGFILE"

   # Lancement du reverse proxy
   echo -e "Lancement du reverse-proxy... En cours"
   # Récupération éventuelle des nouvelles images
   {
      docker pull reseaucerta/nginx:"$TAG"
      docker pull reseaucerta/docker-gen:"$TAG"
   } >>"$LOGFILE" 2>&1
   cd "$DOSSIER_RP" || exit
   docker-compose up -d >>"$LOGFILE"

   if [ $? = 0 ]; then
      echo -e "Lancement du reverse-proxy... Succès"
      echo -e "Le reverse proxy a été lancé.\n" >>"$LOGFILE"
   else
      echo -e "$COLSTOP"
      echo -e "Lancement du reverse-proxy... Échec"
      echo -e "$COLCMD"
      echo -e "Consultez les log \"$LOGFILE\".\n"
      echo -e "Le reverse proxy n'a pas pu être lancé." >>"$LOGFILE"
      exit
   fi

   # Création ou mise à jour du certificat.
   # Si un certificat est fourni dans le param.conf on l'utilise.
   # sinon on le crée si le certficat existant ne correspond pas aux paramètres de param.conf
   # À FAIRE
   CREATE_CERTIFICAT_NGINX

   # Registry

   #Lancement du registry
   echo -e "$COLDEFAUT"
   echo -e "Lancement du registry"
   echo -e "$COLCMD"

   # Si le registre est lancé avec le bon port et un certificat valide on maintient le service sinon on le recrée
   if docker ps | grep e-combox_registry | grep "$PORT_REGISTRY" >/dev/null; then
      VERIF_CERTIFICAT_REGISTRY
      if [ "${verif_cert_registry:?}" = "true" ]; then
         echo -e "Registry... Déjà lancé.\n"
         echo -e "Le registry existe.\n" >>"$LOGFILE"
      fi
   else
      echo -e "Il n'y a pas de registry lancé avec le bon certificat et/ou le bon port.\n" >>"$LOGFILE"
      # On supprime tout registry lancé
      if (docker ps -a | grep e-combox_registry >/dev/null); then
         docker rm -f e-combox_registry >>"$LOGFILE" 2>&1
         # Debug
         #echo -e "Le registry existe mais n'est pas correctement lancé, il sera supprimé puis recréé."
         echo -e "Le registry existe mais n'est pas correctement lancé, il sera supprimé puis recréé." >>"$LOGFILE"
         #echo -e "$COLCMD"
      fi

      # Création des certificats pour le registry
      CREATE_CERTIFICAT_REGISTRY

      # Lancement du registry
      echo -e "Lancement du registry... En cours"
      docker run -d -p "$PORT_REGISTRY":443 --restart=always --name e-combox_registry -v "$DOSSIER_CERTS"/registry:/certs -v registry_data:/var/lib/registry -e VIRTUAL_HOST=registry -e REGISTRY_STORAGE_DELETE_ENABLED=true -e REGISTRY_HTTP_ADDR=0.0.0.0:443 -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/localhost.crt -e REGISTRY_HTTP_TLS_KEY=/certs/localhost.key -e VIRTUAL_PROTO=https -e VIRTUAL_PORT=443 reseaucerta/registry:"$TAG" >>"$LOGFILE" 2>&1
      if [ $? -eq 0 ]; then
         echo -e "Démarrage du registry... Succès"
         echo -e "Le registry a été lancé.\n" >>"$LOGFILE"
      else
         echo -e "$CLOSTOP"
         echo -e "Démarrage du registry... Échec"
         echo -e "$COLCMD"
         exit
      fi
      # Déconnexion du registry au réseau de l'e-combox
      docker network disconnect bridge e-combox_registry >>"$LOGFILE" 2>&1
      # Connexion du registry au réseau de l'e-combox
      docker network connect bridge_e-combox e-combox_registry >>"$LOGFILE" 2>&1

   fi

   if docker network inspect bridge bridge_e-combox | grep e-combox_registry >/dev/null; then
      echo -e "Le réseau de l'e-combox est correctement rattaché au conteneur e-combox_registry." >>"$LOGFILE"
   else
      {
         echo -e "Rattachement du réseau de l'ecombox au conteneur e-combox_registry"
         # Déconnexion du registry au réseau de l'e-combox
         docker network disconnect bridge e-combox_registry
         # Connexion du registry au réseau de l'e-combox
         docker network connect bridge_e-combox e-combox_registry
      } >>"$LOGFILE" 2>&1
   fi

   #lancement du serveur git
   echo -e "$COLDEFAUT"
   echo -e "Lancement du serveur git"
   echo -e "$COLCMD"

   echo -e "Lancement du serveur git... En cours"
   if docker ps | grep reseaucerta/git-http-server:"$TAG" >/dev/null; then
      echo -e "Serveur Git... Déjà lancé"
      echo -e "Rien à faire. Le serveur git local existe et est lancé.\n" >>"$LOGFILE"
   else
      echo -e "Il n'y a pas de serveur git lancé. Lancement du serveur.\n" >>"$LOGFILE"

      #if docker ps -a | grep reseaucerta/git-http-server:"$TAG" || docker ps | grep e-combox_gitserver; then
      if docker ps -a | grep e-combox_gitserver >/dev/null; then
         {
            docker stop e-combox_gitserver
            docker rm e-combox_gitserver
            docker volume rm e-combox_git-data
            docker run -d -p 443 --restart=always --name e-combox_gitserver -v e-combox_git-data:/git -e VIRTUAL_HOST=git -e VIRTUAL_PROTO=https -e VIRTUAL_PORT=443 --network=bridge_e-combox reseaucerta/git-http-server:"$TAG"
         } >>"$LOGFILE" 2>&1

         if [ $? != 0 ]; then
            echo -e "$COLSTOP"
            echo -e "Redémarrage du serveur Git... Échec."
            echo -e "Le serveur git existait mais il a été supprimé et recréé car il n'était pas correctement lancé.\n. Il n'a toujours pas pas pu être lancé.\n" >>"$LOGFILE"
            echo "$COLCMD"
            echo -e "Consultez les log \"$LOGFILE\"."
            exit
         else
            docker exec e-combox_gitserver crond &
            echo -e "Redémarrage du serveur Git... Succès.\n"
            echo -e "Le serveur git existait mais il a été supprimé et recréé car il était soit mal en point, soit sur une mauvaise version. Il a correctement été lancé.\n" >>"$LOGFILE"
         fi
      else
         docker run -d -p 443 --restart=always --name e-combox_gitserver -v e-combox_git-data:/git -e VIRTUAL_HOST=git -e VIRTUAL_PROTO=https -e VIRTUAL_PORT=443 --network=bridge_e-combox reseaucerta/git-http-server:"$TAG" >>"$LOGFILE" 2>&1
         if [ $? != 0 ]; then
            echo -e "$COLSTOP"
            echo -e "Démarrage du serveur Git... Échec"
            echo -e "Le serveur git n'a pas été lancé.\n" >>"$LOGFILE"
            echo "$COLCMD"
            echo -e "Consultez les log \"$LOGFILE\"."
            exit
         else
            docker exec e-combox_gitserver crond &
            echo -e "Démarrage du serveur Git... Succès"
            echo -e "Le serveur Git a été lancé.\n" >>"$LOGFILE"
         fi
      fi
   fi

   # Suppression éventuelle des paramètres du proxy avant le démarrage de Portainer (il ne faut pas que Portainer démarre avec un proxy)
   if [ -e ~/.docker/config.json ]; then
      sed -i.bak "/\"proxies\": {/,9d" ~/.docker/config.json
      if [ "$(sed -n '$=' /root/.docker/config.json)" -le 3 ]; then
         echo "Le fichier config.json est supprimé pour Portainer." >>"$LOGFILE"
         rm -rf ~/.docker/config.json
      fi
   fi

   # Portainer
   echo -e "$COLDEFAUT"
   echo -e "Lancement de Portainer"
   echo -e "$COLCMD"

   #Récupération de portainer
   if [ -d "$DOSSIER_PORTAINER" ]; then
      echo -e "Portainer existe et va être remplacé.\n" >>"$LOGFILE"
      cd "$DOSSIER_PORTAINER" || exit
      docker-compose down >>"$LOGFILE" 2>&1
      cd ..
      rm -rf "$DOSSIER_PORTAINER"
      #docker volume rm e-combox_portainer_portainer-data >>"$LOGFILE" 2>&1
   fi

   cd $DOSSIER_INSTALL || exit
   echo -e "Téléchargement de Portainer... En cours"
   git clone -b $BRANCHE "$DEPOT_GITLAB_PORTAINER".git >>"$LOGFILE" 2>&1

   if [ $? -eq 0 ]; then
      echo -e "Téléchargement de Portainer... Succès"
   else
      echo -e "$COLSTOP"
      echo -e "Téléchargement de Portainer... Échec"
      echo -e "$COLCMD"
      echo -e "Consultez les log \"$LOGFILE\".\n"
      echo -e "Portainer n'a pas pu être téléchargé." >>"$LOGFILE"
      exit
   fi

   #Configuration de l'adresse IP
   echo -e "PORT=$PORT_PORTAINER" >"$DOSSIER_PORTAINER"/.env
   echo -e "Mise à jour de $DOSSIER_PORTAINER/.env... Fait"
   echo -e "Mise à jour de $DOSSIER_PORTAINER/.env... Fait.\n" >>"$LOGFILE"

   # Lancement de Portainer
   cd "$DOSSIER_PORTAINER" || exit
   # On force le téléchargement de Portainer dans la dernière version (en dev uniquement)
   #docker pull portainer/portainer-ce:latest
   docker-compose up -d >>"$LOGFILE"

   if [ $? = 0 ]; then
      echo -e "Lancement de Portainer... Succès"
      echo -e "Portainer a été lancé.\n" >>"$LOGFILE"
   else
      echo -e "$COLSTOP"
      echo -e "Lancement de Portainer... Échec"
      echo -e "$COLCMD"
      echo -e "Consultez les log \"$LOGFILE\".\n"
      echo -e "Portainer n'a pas pu être lancé." >>"$LOGFILE"
      exit
   fi

   # Attente pour que Portainer soit complètement opérationnel
   sleep 5

   # Connexion à l'API
   CONNECTE_API
   if [ "${connect_api:?}" = "false" ]; then
      exit
   fi

   # Réintégration du config.json

   if [ -e ~/.docker/config.json.bak ]; then
      mv ~/.docker/config.json.bak ~/.docker/config.json
   fi

   # Configuration de l'application
   echo -e "$COLDEFAUT"
   echo -e "Configuration d'e-comBox"
   echo -e "$COLCMD"
   # Appel de la fonction qui configure l'application
   CONF_ECB

   # Suppression éventuelle des images
   # Si une option spécifique a été passée en ligne de commande, la valeur de DEL_IMAGES est modifiée juste pour cette exécution de script
   if [ "$del_images_o" = "true" ]; then
      DEL_IMAGES="true"
   elif [ "$del_images_o" = "false" ]; then
      DEL_IMAGES="false"
   fi

   if [ "$DEL_IMAGES" = "true" ]; then
      # Suppression des images non associées à un site en cours d'exécution
      for image in $(docker images --format "{{.Repository}}:{{.Tag}}"); do
         if (docker rmi "$image" >>"$LOGFILE" 2>&1); then
            echo -e "Suppression de $image... Fait"
         fi
      done >>"$LOGFILE" 2>&1
      echo -e "Images non associées à un site... Supprimées"
   fi

   # Suppression des images orphelines (image qui n'a pas de conteneur associé, mais qui est toujours référencée par l'image parente)
   if [ "$(docker images -qf dangling=true)" ]; then
      # shellcheck disable=SC2046
      docker rmi $(docker images -qf dangling=true) >>"$LOGFILE" 2>&1
      echo -e "Images orphelines... Supprimées"
   fi

   # Arrêt des stacks
   if [ ! -e $DOSSIER_INSTALL/version2 ]; then
      echo -e "Arrêt des stacks démarrés... En cours"
      STOP_STACKS
   fi

   # Gestion de l'équipe Profs
   echo -e "$COLDEFAUT"
   echo -e "Gestion de l'équipe \"Profs\""
   echo -e "Gestion de l'équipe \"Profs\"" >>"$LOGFILE"
   echo -e "$COLCMD"

   GESTION_PROFS

   # Ajout du stack (s'il n'existe pas) pour les mentions légales : ce stack doit uniquement accueillir les fichiers nécessaires.
   echo -e "$COLDEFAUT"
   echo -e "Gestion du stack FSserver pour les mentions légales"
   echo -e "Gestion du stack FSserver pour les mentions légales" >>"$LOGFILE"
   echo -e "$COLCMD"

   STACK_FSSERVER=$(curl --noproxy "*" -s -k -H "Authorization: Bearer $T" "$P_URL/api/stacks" | jq -c '.[] | select(.Name == "fsserver")')
   if [ -n "$STACK_FSSERVER" ]; then
      # Le stack a déjà été créé (même s'il n'est pas démarré)
      echo -e "Le stack FSserver pour les mentions légales a déjà été créé." >>"$LOGFILE"
      echo -e "Stack FSserver pour les mentions légales... Présent"
   else
      # le stack n'existe pas, il faut le créer
      echo "Création du stack \"FSserver\"..." >>"$LOGFILE"
      CREATE_STACK_FS=$(
         curl --noproxy "*" -s -k \
            "$P_URL/api/stacks?type=2&method=repository&endpointId=1" \
            -X POST \
            -H "Authorization: Bearer $T" -H "Content-Type: application/json" \
            -d "{\"name\": \"FSserver\",\"composeFile\": \"docker-compose-FSserver.yml\",\"repositoryURL\": \"$DEPOT_GITLAB_DC\",\"repositoryReferenceName\": \"refs/heads/$BRANCHE\",\"repositoryAuthentication\": false}"
      )
      STACK_FSSERVER=$CREATE_STACK_FS
      echo "Retour de la création du stack : $CREATE_STACK_FS" >>"$LOGFILE"
      echo -e "Stack FSserver pour les mentions légales... Créé"
   fi

   # Démarrage du stack via l'api portainer si celui-ci n'est pas déjà lancé
   echo -e "Démarrage du stack FSserver pour les mentions légales... En cours"
   status_stack=$(echo "$STACK_FSSERVER" | jq -r '.Status')
   ID_STACK_FSSERVER=$(echo "$STACK_FSSERVER" | jq -r '.Id')

   if [ "$status_stack" = "1" ]; then
      # le stack est démarré
      echo -e "Le stack FSserver est démarré." >>"$LOGFILE"
   else
      # le stack n'est pas démarré, il sera démarré via l'api de portainer
      echo -e "Le satck FSserver n'est pas démarré, il faut le démarrer..." >>"$LOGFILE"
      echo "Démarrage du stack \"FSserver\"..." >>"$LOGFILE"
      START_STACK_FSSERVER=$(
         curl --noproxy "*" -s -k \
            "$P_URL/api/stacks/$ID_STACK_FSSERVER/start" \
            -X POST \
            -H "Authorization: Bearer $T"
      )
      echo "Retour du démarrage du stack FSserver: $START_STACK_FSSERVER" >>"$LOGFILE"
   fi

   # Création d'une nouvelle restriction (association du stack au groupe Profs) si aucune restriction n'existe (sinon mis à jour de la restriction)
   RESTRICTION_FSSERVER=$(curl --noproxy "*" -s -k \
      "$P_URL/api/stacks/$ID_STACK_FSSERVER" \
      -X GET \
      -H "Authorization: Bearer $T" | grep ResourceControl)

   if [ -z "$RESTRICTION_FSSERVER" ]; then
      AJOUT_RESTRICTION_FSSERVER=$(
         curl --noproxy "*" -s -k \
            "$P_URL/api/resource_controls" \
            -X POST \
            -H "Authorization: Bearer $T" \
            -d "{\"administratorsOnly\":false,\"public\":false,\"teams\":[$ID_GROUPE_PROFS],\"type\":6,\"resourceId\":\"1_fsserver\"}"
      )

      echo -e "Retour AJOUT_RESTRICTION_FSSERVER : $AJOUT_RESTRICTION_FSSERVER" >>"$LOGFILE"

   else
      # Récupération de l'ID de restriction
      ID_RESTRICTION_FSSERVER=$(echo "$RESTRICTION_FSSERVER" | jq -r '.ResourceControl.Id')
      # Mise à jour de la restriction
      MAJ_RESTRICTION_FSSERVER=$(
         curl --noproxy "*" -s -k \
            "$P_URL/api/resource_controls/$ID_RESTRICTION_FSSERVER" \
            -X PUT \
            -H "Authorization: Bearer $T" \
            -d "{\"administratorsOnly\":false,\"public\":false,\"teams\":[$ID_GROUPE_PROFS],\"type\":6}"
      )
      echo -e "Retour MAJ_RESTRICTION_FSSERVER : $MAJ_RESTRICTION_FSSERVER" >>"$LOGFILE"

   fi
   echo -e "Stack pour les mentions légales FSserver... Démarré"

   echo -e "$COLTITRE"
   echo "***************************************************"
   echo "         FIN DE L'INSTALLATION DE E-COMBOX         "
   echo "***************************************************"
   echo -e "$COLCMD"

   # Nettoyage des anciens scripts
   if [ -e $DOSSIER_INSTALL/delete_conteneurs.sh ]; then
      rm $DOSSIER_INSTALL/delete_conteneurs.sh
   fi

   if [ -e $DOSSIER_INSTALL/e-comBox_identifiants_acces_applications.pdf ]; then
      rm $DOSSIER_INSTALL/e-comBox_identifiants_acces_applications.pdf
   fi

   # Téléchargement du CHANGELOG.md, de la licence, du README.md et des scripts utiles à la configuration et à l'administration
   echo -e "Télécharchement des scripts utiles... En cours"
   curl -fsSL https://forge.aeif.fr/e-combox/e-combox_webapp/-/raw/master/CHANGELOG.md -o $DOSSIER_INSTALL/CHANGELOG.md
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/LICENCE -o $DOSSIER_INSTALL/LICENCE
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/README.md -o $DOSSIER_INSTALL/README.md
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/configure_application.sh -o $DOSSIER_INSTALL/configure_application.sh
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/sauv_sites.sh -o $DOSSIER_INSTALL/sauv_sites.sh
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/restaure_sites.sh -o $DOSSIER_INSTALL/restaure_sites.sh
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/restaure_v3.sh -o $DOSSIER_INSTALL/restaure_v3.sh
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/delete_application.sh -o $DOSSIER_INSTALL/delete_application.sh
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/reinitialise_application.sh -o $DOSSIER_INSTALL/reinitialise_application.sh
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/reset_pass_portainer.sh -o $DOSSIER_INSTALL/reset_pass_portainer.sh
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/manage_certificats.sh -o $DOSSIER_INSTALL/manage_certificats.sh
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/manage_images.sh -o $DOSSIER_INSTALL/manage_images.sh
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/update_ecb.sh -o $DOSSIER_INSTALL/update_ecb.sh

   echo -e "Télécharchement des scripts utiles... Fait"
   echo -e "Télécharchement des scripts utiles... Fait" >>"$LOGFILE"

   # Restriction des droits du dossier /opt/e-combox
   chmod 660 "$DOSSIER_INSTALL"
   echo -e "Restriction des droits du dossier /opt/e-combox... Fait"
   echo -e "Restriction des droits du dossier /opt/e-combox... Fait" >>"$LOGFILE"

   # Migration des sites si la v3 est installée
   if [ -e $DOSSIER_INSTALL/version3 ]; then
      # Migration des sites
      echo -e "$COLTITRE"
      echo "***************************************************"
      echo "*              MIGRATION DES SITES                *"
      echo "***************************************************"
      UPDATE_STACKS

      # Suppression du fichier témoin
      rm $DOSSIER_INSTALL/version3

      # Suppression des images de la version 3
      for image in $(docker images --filter=reference='reseaucerta/*:3.0' --format "{{.Repository}}:{{.Tag}}"); do
         docker rmi "$image"
      done >>"$LOGFILE" 2>&1

      # Suppression des images locales personnalisées
      for image in $(docker images --filter=reference='localhost:*/*' --format "{{.Repository}}:{{.Tag}}"); do
         docker rmi "$image"
      done >>"$LOGFILE" 2>&1

      # Suppression des images inutilisées
      if [ "$(docker images -qf dangling=true)" ]; then
         # shellcheck disable=SC2046
         docker rmi $(docker images -qf dangling=true) >>"$LOGFILE" 2>&1
      fi

      # Ménage des scripts devenus inutiles
      rm "$DOSSIER_INSTALL"/ajout_auth.sh "$DOSSIER_INSTALL"/suppr_auth.sh "$DOSSIER_INSTALL"/restaure_v2.sh "$DOSSIER_INSTALL"/change_config_ip.sh "$DOSSIER_INSTALL"/migrate_application.sh "$DOSSIER_INSTALL"/sync_pwd_portainer.sh

   fi

   # Pour permettre aux utilisateurs non admin de créer des stacks (option qui change à la migration)
   MODIF_SETTINGS_PORTAINER=$(
      curl --noproxy "*" -s -k \
         "$P_URL/api/endpoints/1/settings" \
         -H "Authorization: Bearer $T" -X PUT \
         -H "Content-Type: application/json;charset=UTF-8" \
         -H 'Cache-Control: no-cache' \
         -d "{\"allowSysctlSettingForRegularUsers\":true}"
   )
   echo -e "Retour MODIF_SETTINGS_PORTAINER : $MODIF_SETTINGS_PORTAINER" >>"$LOGFILE"

   # Affichage des URL
   NOUVELLE_URL_PORTAINER=$FQDN/portainer/
   NOUVELLE_URL_APP=$FQDN/app/
   NOUVELLE_URL_SITE=$FQDN/nom_du_site/

   echo -e "$COLINFO"
   echo -e "L'application e-comBox est accessible à l'URL :"
   echo -e "${COLCHOIX}https://$NOUVELLE_URL_APP"
   echo -e "$COLINFO"
   echo "Portainer est accessible à l'URL :"
   echo -e "${COLCHOIX}https://$NOUVELLE_URL_PORTAINER"
   echo -e "$COLINFO"
   echo -e "Les sites seront accessibles via des URL formées de la manière suivante :"
   echo -e "${COLCHOIX}https://$NOUVELLE_URL_SITE"
   echo -e "$COLCMD"

   echo -e "\nVous pouvez accéder à l'interface d'e-comBox via le compte admin de Portainer mais une bonne pratique consiste à créer, sur Portainer, au moins un compte dans le groupe \"Profs\" puis accéder à l'interface d'e-comBox avec les identifiants de ce dernier."

   {
      echo -e "Installation terminée"
      echo -e "Application e-comBox : https://$NOUVELLE_URL_APP"
      echo -e "Portainer : https://$NOUVELLE_URL_PORTAINER"
      echo -e "Les sites : https://$NOUVELLE_URL_SITE"

      echo -e "\nVous pouvez accéder à l'interface d'e-comBox via le compte admin de Portainer mais une bonne pratique consiste à créer, sur Portainer, au moins un compte dans le groupe \"Profs\" puis accéder à l'interface d'e-comBox avec les identifiants de ce dernier."

   } >>"$LOGFILE"

}

# Vérification de la version pour réaliser une éventuelle migration
verifVersion() {
   # Vérification d'anciennes versions
   if docker ps -a | grep e-combox:1.0; then
      echo -e "$COLINFO"
      echo -e "**** ATTENTION ****"
      echo -e "**** VERSION 1 DÉTECTÉE ****" >>"$LOGFILE"
      echo -e "Le système constate que la version 1 est installée. La version des sites est incompatible, ces derniers seront supprimés."
      echo -e "Le système constate que la version 1 est installée. La version des sites est incompatible, ces derniers seront supprimés." >>"$LOGFILE"
      # Nettoyage complet des éléments de Docker
      CONTENEURS_ACTIFS=$(docker ps -q)
      if [ -n "$CONTENEURS_ACTIFS" ]; then
         docker stop "$CONTENEURS_ACTIFS" >>"$LOGFILE" 2>&1
      fi
      docker system prune -f -a --volumes >>"$LOGFILE" 2>&1
      echo -e "$COLINFO"
      echo -e "Suppression des sites effectués. Le système va procéder à la mise à jour de l'e-combox vers la version 4."
      echo -e "Suppression des sites effectués. Le système va procéder à la mise à jour de l'e-combox vers la version 4." >>"$LOGFILE"
      echo -e "$COLCMD"
      installer
   else
      if docker ps -a | grep e-combox:2.0 >>"$LOGFILE"; then
         echo -e "$COLINFO"
         echo -e "**** ATTENTION ****\n"
         echo -e "Le système constate que la version 2 est installée. Installez la version 3 avant de passer à la version 4."
         echo -e "**** ATTENTION ****\n" >>"$LOGFILE"
         echo -e "Le système constate que la version 2 est installée. Installez la version 3 avant de passer à la version 4." >>"$LOGFILE"
         echo -e "$COLCMD"
         exit 1
      else
         if docker ps -a | grep e-combox:3.0 >>"$LOGFILE"; then
            echo -e "$COLINFO"
            echo -e "**** ATTENTION ****\n"
            echo -e "Le système constate que la version 3 est installée. Une migration des sites va être effectuée, celle-ci est sans danger."
            echo -e "**** ATTENTION ****\n" >>"$LOGFILE"
            echo -e "Le système constate que la version 3 est installée. Une migration des sites va être effectuée, celle-ci est sans danger." >>"$LOGFILE"
            touch $DOSSIER_INSTALL/version3
            echo -e "$COLCMD"
            installer
         else
            installer

         fi
      fi
   fi
   echo -e "$COLCMD"
}

initInstall
