#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

# Appel des fichiers de variables, de fonctions et de param.conf
DOSSIER_INSTALL="/opt/e-comBox"
source "$DOSSIER_INSTALL"/variables.sh
source "$DOSSIER_INSTALL"/fonctions.sh
source "$DOSSIER_INSTALL"/param.conf

echo -e "\nMise à jour de l'e-comBox le $(date)\n" >>"$LOGFILE"
CONF_ECB