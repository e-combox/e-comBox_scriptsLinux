#!/bin/bash

# Définition de la branche
BRANCHE="4.1"

# Téléchargement et lancement du script qui installe e-comBox
wget -q https://forge.aeif.fr/e-combox/e-comBox_scriptsLinux/raw/$BRANCHE/configure_application.sh --output-document configure_application.sh
bash configure_application.sh "$@"
if ! [ "$PWD" = "/opt/e-combox" ]; then
    rm configure_application.sh
fi
