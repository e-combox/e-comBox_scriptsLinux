#!/bin/bash

# Définition de la branche et de la version des applications
BRANCHE="4.1"

# Récupération sur gitlab du fichier qui va effectivement configurer l'application
wget -q  https://forge.aeif.fr/e-combox/e-comBox_scriptsLinux/raw/$BRANCHE/start_configure_application.sh --output-document start_configure_application.sh

# Exécution du fichier
bash start_configure_application.sh "$@"

# Suppression du fichier
rm start_configure_application.sh
