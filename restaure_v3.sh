#!/bin/bash
# shellcheck source=/dev/null

# Définition du dossier d'installation
DOSSIER_INSTALL=/opt/e-comBox

# Appel du fichier de fonctions
source $DOSSIER_INSTALL/fonctions.sh

# Récupération sur gitlab du fichier de fonctions
#curl -fsSL https://gitlab.com/e-combox/e-comBox_scriptsLinux/raw/$BRANCHE/fonctions.sh -o $DOSSIER_INSTALL/fonctions.sh

# Restauration des sites de la v3

echo -e "$COLINFO"
echo -e "Vous vous apprếtez à restaurer la version 3. Attention, la procédure peut être un peu longue."

# Choix de l'archive à restaurer
echo -e "$COLTXT"
echo -e "L'archive est stockée par défaut dans le dossier $DOSSIER_INSTALL/sauv."
echo -e "Voulez-vous choisir un autre dossier ? (n par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
read -r REPONSE_DOSSIER
if [ -z "$REPONSE_DOSSIER" ] || [ "$REPONSE_DOSSIER" = "n" ] || [ "$REPONSE_DOSSIER" = "N" ]; then
  REPONSE_DOSSIER="n"
  DOSSIER_SOURCE=$DOSSIER_INSTALL/sauv
  if (! dialog 2>/dev/null); then
    apt update
    apt install dialog
  fi
  ARCHIVE=$(dialog --stdout --title "Choisissez l'archive à partir de laquelle vous voulez restaurer les sites. Le nom de l'archive peut directement être saisi ou peut être choisi en utilisant les flèches et la touche de tabulation du clavier. Un élément est validé avec la barre d'espacement." --fselect $DOSSIER_SOURCE/ 14 48)
  VALIDE_ARCHIVE
  echo "Le nom de l'archive est $ARCHIVE"
fi
if [ "$REPONSE_DOSSIER" != "n" ] && [ "$REPONSE_DOSSIER" != "n" ]; then
  # Parcours des dossiers accessibles
  ARCHIVE=$(dialog --stdout --title "Choisissez l'archive à partir de laquelle vous voulez restaurer les sites. Le chemin vers le dossier voulu peut directement être saisi ou peut être choisi en utilisant les flèches et la touche de tabulation du clavier. Un élément est validé avec la barre d'espacement." --fselect / 14 48)
  # Validation de l'archive en gérant un éventuel "annuler"
  if [ -z "$ARCHIVE" ]; then
    echo -e "\nVous n'avez pas choisi d'archive à restaurer. Veuillez relancez le script pour sélectionner une archive à restaurer."
    sleep 2
    exit
  else
    echo -e "\nVous avez choisi l'archive ${ARCHIVE}. Validez-vous ce choix ? (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
    read -r REPONSE_ARCHIVE
    if [ -z "$REPONSE_ARCHIVE" ]; then
      REPONSE_ARCHIVE="o"
    fi
    if [ "$REPONSE_ARCHIVE" != "o" ] && [ "$REPONSE_ARCHIVE" != "O" ]; then
      echo -e "\nVeuillez relancez le script pour sélectionner une archive à restaurer."
      sleep 2
      exit
    fi
  fi
fi

# Suppression des conteneurs, volumes, réseaux et images
STOP_STACKS
#shellcheck disable=SC2046
docker stop $(docker ps -aq)
docker system prune -a -f --volumes

# Restauration des sites
echo -e "$COLTXT"
echo -e "Les sites vont être restaurés. Veuillez patienter."
cd /var/lib/docker/ || exit
tar -xzf "$ARCHIVE" --preserve-permissions --same-owner 2>>"$DOSSIER_SOURCE"/sauv.log

# Suppression des dossiers et fichiers
rm -rf "$DOSSIER_INSTALL"/e-comBox_portainer "$DOSSIER_INSTALL"/e-comBox_reverseproxy "$DOSSIER_INSTALL"/certs
rm "$DOSSIER_INSTALL"/param.conf

# Réinstallation de la version 3 de l'application
echo -e "La version 3 de l'e-comBox doit être réinstallée"
BRANCHE=v3
curl -fsSL https://gitlab.com/e-combox/e-comBox_scriptsLinux/raw/$BRANCHE/configure_application.sh -o configure_application.sh
bash configure_application.sh

# Nettoyage du dossier d'installation
rm -rf $DOSSIER_INSTALL/migration

exit
