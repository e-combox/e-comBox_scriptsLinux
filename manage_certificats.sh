#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

# Appel des fichiers de variables, fonctions et param.conf
DOSSIER_INSTALL="/opt/e-comBox"
source "$DOSSIER_INSTALL"/variables.sh
source "$DOSSIER_INSTALL"/fonctions.sh
source "$FICHIER_PARAM"
#source ./variables.sh
#source ./fonctions.sh

SHOW_USAGE() {
    echo -e "\nCe script permet la création et la mise à jour du certificat pour l'e-comBox."
    echo -e "Des options doivent être passées en ligne de commande (voir Usage ci-après)."
    echo -e "Usage: bash $0 -c|u [-m \"valeur\"] [-a] [-h]"
    echo -e "\t-c\t\tCréation et mise en place d'un certificat Lets'encrypt"
    echo -e "\t-u\t\tMise à jour du certificat à partir des paramètres du fichier param.conf"
    echo -e "\t-m\t\tAdresse de courriel  [-m \"adresse courriel\"], non obligatoire"
    echo -e "\t-a\t\tInstallation de la mise à jour automatique du certificat lorsque celui-ci est expiré. Cette option n'a d'utilité qu'une seule fois"
    echo -e "\t-h\t\tDétail des options"
}

if [ $# -eq 0 ]; then
    echo -e "${COLSTOP}Une des deux options \"-c\" ou \"-u\" doit obligatoirement être renseignée.\n${COLCMD}"
    echo -e "\nAucun option passée en ligne de commande.\n" >>"$LOGFILE"
    SHOW_USAGE
    exit
fi

# Initialisation des paramètres passés au script
while getopts "cum:ah" option; do
    # c: création d'un certificat Lets'encrypt
    # u: mise à jour du certificat s'il est expiré ou selon les paramètres du param.conf
    # m: adresse de courriel
    # s: mise en place de cron pour mettre à jour automatiquement le certificat quand il est périmé
    # h: help

    case $option in
    c) create="true" ;;
    u) update="true" ;;
    m) mail_o=$OPTARG ;;
    a) cron="true" ;;
    h)
        SHOW_USAGE
        exit
        ;;
    \?)
        echo -e "\n$COLSTOP $OPTARG : option invalide.$COLCMD\n"
        echo -e "\n$COLSTOP $OPTARG : option invalide.$COLCMD\n" >>"$LOGFILE"
        SHOW_USAGE
        exit 1
        ;;
    esac
done

echo -e "$COLDEFAUT"
echo -e "Configuration des certificats d'e-comBox le $(date)"
echo -e "$COLCMD"
echo -e "\nConfiguration des certificats d'e-comBox le $(date)\n" >>"$LOGFILE"

# Pour empêcher l'utilisation simultanée des options "-c" et "-u"
if [ "$create" ] && [ "$update" ]; then
    echo -e "${COLSTOP}Les options \"-c\" et \"-u\" ne peuvent pas s'utiliser simultanément\n${COLCMD}."
    SHOW_USAGE
    exit
fi

# Pour forcer l'utilisation d'une des deux options "-c" ou "-u"
if [ -z "$create" ] && [ -z "$update" ]; then
    echo -e "${COLSTOP}Une des deux options \"-c\" ou \"-u\" doit obligatoirement être utilisée\n${COLCMD}."
    SHOW_USAGE
    exit
fi

# Création d'un certificat Lets'Encrypt
if [ "$create" ]; then

    # Vérification du mail (non obligatoire pour créer un certificat Lets'Encrypt)
    if [[ -n $mail_o && "$mail_o" =~ [a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,} ]]; then
        sed -i "s|^MAIL=.*|MAIL=\"$mail_o\"|" "$FICHIER_PARAM"
        MAIL=$mail_o
    fi

    # Création du certificat Lets'Encrypt si le nom de domaine est valide
    if [ -n "$DOMAINE" ] && [[ "$DOMAINE" =~ [A-Za-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[A-Za-z]{2,} ]]; then
        echo -e "Certificat Let's Encrypt... En cours de création"
        # Création du certificat (le mail est facultatif ==> si pas de mail --register-unsafely-without-email)
        if [ -n "$MAIL" ]; then
            docker run -it --rm --name certbot -p 80:80 -p 443:443 -v "/opt/e-comBox/letsencrypt:/etc/letsencrypt" certbot/certbot certonly --agree-tos --standalone --email "$MAIL" -d "$DOMAINE" >>"$LOGFILE" 2>&1
        else
            docker run -it --rm --name certbot -p 80:80 -p 443:443 -v "/opt/e-comBox/letsencrypt:/etc/letsencrypt" certbot/certbot certonly --agree-tos --register-unsafely-without-email --standalone -d "$DOMAINE" >>"$LOGFILE" 2>&1
        fi

        # Intégration des chemins vers le certificat et la clé dans le param.conf
        CHEMIN_CERT="/opt/e-comBox/letsencrypt/live/ecb.cub.corsica/fullchain.pem"
        CHEMIN_KEY="/opt/e-comBox/letsencrypt/live/ecb.cub.corsica/privkey.pem"
        if [ -n "$CHEMIN_CERT" ]; then
            sed -i "s|CHEMIN_CERT=.*|CHEMIN_CERT=\"$CHEMIN_CERT\"|g" "$FICHIER_PARAM"
            sed -i "s|CHEMIN_KEY=.*|CHEMIN_KEY=\"$CHEMIN_KEY\"|g" "$FICHIER_PARAM"
            echo -e "Certificat Let's Encrypt... Créé"
            echo -e "Chemin du certificat... $CHEMIN_CERT"
            USE_CERTIFICAT_NGINX
        else
            echo -e "$COLSTOP"
            echo -e "Le certificat Lets'Encrypt n'a pas été installé. Consultez le fichier /var/log/ecombox.log."
            echo -e "Le certificat Lets'Encrypt n'a pas été installé." >>"$LOGFILE"
            echo -e "$COLCMD"
            exit
        fi
    else
        echo -e "$COLSTOP"
        echo -e "Le nom de domaine n'est pas valide, le certificat Let's Encrypt ne peut pas être créé."
        echo -e "Le nom de domaine n'est pas valide, le certificat Let's Encrypt ne peut pas être créé." >>/"$LOGFILE"
        echo -e "$COLCMD"
        exit
    fi
fi

# Mis à jour des certificats
if [ "$update" ]; then
    # Mise à jour du certificat Let's Encrypt
    if [[ -n "$CHEMIN_CERT" && -n "$CHEMIN_KEY" ]]; then
        if (openssl x509 -in "$CHEMIN_CERT" -noout -text | grep "Let's Encrypt"); then
            docker run -it --rm --name certbot -p 80:80 -p 443:443 -v "/opt/e-comBox/letsencrypt:/etc/letsencrypt" certbot/certbot renew >>"$LOGFILE" 2>&1
        fi
        # Reload conf nginx
        USE_CERTIFICAT_NGINX
    else
        # Mise à jour des certificats pour le regisry
        CREATE_CERTIFICAT_NGINX
    fi

    # Mise à jour des certificats pour le registry
    CREATE_CERTIFICAT_REGISTRY
fi

if [ "$cron" ]; then
    # Tous les jours, le crontab lance le script cron_certificat.sh qui regarde si les certificats doivent être mis à jour.
    if ! [ -e /etc/cron.daily/cron_certificats.sh ]; then
        # Récupération du fichier cron_certificat.sh
        curl -fsSL "$DEPOT_GITLAB_SCRIPTS"/raw/"$BRANCHE"/cron_certificat.sh -o /etc/cron.daily/cron_certificat.sh
        chmod ug+x /etc/cron.daily/cron_certificat.sh
        echo -e "Renouvellement automatique des certificats... Fait"
        echo -e "Renouvellement automatique des certificats... Fait" >>"$FILELOG"
    else
        echo -e "Renouvellement automatique des certificats... Déjà installé"
        echo -e "Renouvellement automatique des certificats... Déjà installé"  >>"$FILELOG"
    fi
fi
